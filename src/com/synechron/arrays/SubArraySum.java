package com.synechron.arrays;

public class SubArraySum {

	int findSubArray(int arr[], int sum, int n) {
		int currSum = arr[0], start = 0, i;
		for (i = 1; i < n - 1; i++) {
			while (currSum > sum && start < i - 1) {
				currSum = currSum - arr[start];
				start++;
			}

			if (currSum == sum) {
				int p = i - 1;
				System.out.println("Sum found between indexes " + start + " and " + p);
				return 1;
			}

			if (i < n)
				currSum = currSum + arr[i];
		}
		System.out.println("No subarray found with give sum");
		return 0;
	}

	public static void main(String[] args) {
		SubArraySum arraysum = new SubArraySum();
		int arr[] = { 15, 2, 4, 8, 9, 5, 10, 23 };
		int n = arr.length;
		int sum = 23;
		arraysum.findSubArray(arr, sum, n);

	}

}
