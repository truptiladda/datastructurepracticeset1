package com.synechron.arrays;

public class DiagonalSum {

	static void printDiagonalSum(int[][] matrix, int n) {

		int principal = 0;
		int sec = 0;
		for (int i = 0; i < n; i++) {
			principal = principal + matrix[i][i];
			sec = sec + matrix[i][n - i - 1];

		}

		System.out.println("Principal Diagonal:" + principal);

		System.out.println("Secondary Diagonal:" + sec);
	}

	public static void main(String[] args) {
		int[][] a = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 1, 2, 3, 4 }, { 5, 6, 7, 8 } };

		printDiagonalSum(a, 4);

	}

}
