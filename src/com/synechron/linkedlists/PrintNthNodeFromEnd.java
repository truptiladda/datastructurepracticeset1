package com.synechron.linkedlists;

public class PrintNthNodeFromEnd {

	static Node head;

	static class Node {
		int data;
		Node next;

		Node(int d) {
			data = d;
			next = null;
		}
	}

	int printNthFromLast(int n) {

		int len = 0;
		Node temp = head;
		while (temp != null) {
			temp = temp.next;
			len++;

		}

		if (len < n)
			return -1;

		temp = head;
		for (int i = 1; i < len - n + 1; i++) {
			temp = temp.next;

		}
//		System.out.println(temp.data);
		return temp.data;

	}

	public static void main(String args[]) {
		PrintNthNodeFromEnd list = new PrintNthNodeFromEnd();
		list.head = new Node(85);
		list.head.next = new Node(15);
		list.head.next.next = new Node(4);
		list.head.next.next.next = new Node(20);
		System.out.println(list.printNthFromLast(2));

	}

}
