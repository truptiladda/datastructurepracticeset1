package com.synechron.linkedlists;

public class MiddleElementOfLinkedList {

	static Node head;

	static class Node {
		int data;
		Node next;

		Node(int d) {
			data = d;
			next = null;
		}

	}

	static void printMiddle() {
		Node slow = head;
		Node fast = head;

		if (head != null) {
			while (fast != null && fast.next != null) {
				slow = slow.next;
				fast = fast.next.next;

			}

		}
		System.out.println("Middle element is : " + slow.data);
	}

	public static void main(String[] args) {

		MiddleElementOfLinkedList list = new MiddleElementOfLinkedList();
		list.head = new Node(85);
		list.head.next = new Node(15);
		list.head.next.next = new Node(4);
		list.head.next.next.next = new Node(20);
		printMiddle();

	}

}
