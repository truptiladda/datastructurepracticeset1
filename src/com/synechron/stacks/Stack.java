package com.synechron.stacks;

import java.util.LinkedList;
import java.util.Queue;

public class Stack {
	Queue<Integer> q1 = new LinkedList<>();
	Queue<Integer> q2 = new LinkedList<>();
	int current_size;

	Stack() {
		current_size = 0;
	}

	void pop() {
		if (q1.isEmpty()) {
			return;
		}
		while (q1.size() != 1) {
			q2.add(q1.peek());
			q1.remove();
		}

		q1.remove();
		current_size--;

		Queue<Integer> q = q1;
		q1 = q2;
		q2 = q;
	}

	void push(int x) {
		q1.add(x);
		current_size++;
	}

	int top() {

		if (q1.isEmpty()) {
			return -1;
		}
		while (q1.size() != 1) {
			q2.add(q1.peek());
			q1.remove();
		}

		int temp = q1.peek();
		q1.remove();

		q1.add(temp);

		Queue<Integer> q = q1;
		q1 = q2;
		q2 = q;
		return temp;

	}

	int size() {
		return current_size;
	}

	public static void main(String[] args) {

		Stack s = new Stack();
		s.push(5);
		s.push(4);
		s.push(9);
		s.push(10);
		s.push(3);
		s.push(8);
		s.push(12);

		System.out.println("current size: " + s.size());
		System.out.println(s.top());
		s.pop();
		System.out.println(s.top());
		s.pop();
		System.out.println(s.top());
		System.out.println("current size: " + s.size());

	}

}
