package com.synechron.stacks;

import java.time.chrono.MinguoChronology;
import java.util.Stack;

public class StackWithGetMin {

	Stack<Integer> s;
	Integer minElement;

	StackWithGetMin() {
		s = new Stack<Integer>();
	}

	void getMin() {
		if (s.isEmpty()) {
			System.out.println("stack is empty");
		} else {
			System.out.println("Min elemnet is: " + minElement);
		}

	}

	void peek() {
		if (s.isEmpty()) {
			System.out.println("stack is empty");
		}

		Integer top = s.peek();
		if (top < minElement) {
			System.out.println("Top element is " + minElement);
		} else {
			System.out.println("Top element is " + top);
		}
	}

	void pop() {
		if (s.isEmpty()) {
			System.out.println("Stack is empty");
		}

		Integer t = s.pop();

		if (t < minElement) {
			minElement = 2 * minElement - t;
		}

		else {
			System.out.println(t);
		}

	}

	void push(int x) {

		if (s.isEmpty()) {
			minElement = x;
			s.push(x);
			System.out.println("Number inserted is " + x);
			return;
		}
		if (x < minElement) {
			s.push(2 * x - minElement);
			minElement = x;

		} else {
			s.push(x);
		}

		System.out.println("Number inserted is " + x);
	}

	public static void main(String[] args) {
		StackWithGetMin s = new StackWithGetMin();
		s.push(3);
		s.push(5);
		s.getMin();
		s.push(2);
		s.push(1);
		s.getMin();
		s.pop();
		s.getMin();
		s.pop();
		s.peek();
	}

}
