package com.synechron.arrays;

public class ArrayRotation {

	void rotate(int[] arr, int n, int d) {

		for (int i = 0; i < d; i++) {
			rotateOneByOne(arr, n);

		}

		for (int i = 0; i < n; i++)
			System.out.print(arr[i] + " ");

	}

	void rotateOneByOne(int arr[], int n) {
		int temp, j;
		temp = arr[0];
		for (j = 0; j < n - 1; j++)
			arr[j] = arr[j + 1];
		arr[j] = temp;
	}

	public static void main(String[] args) {
		ArrayRotation r = new ArrayRotation();
		int arr[] = { 1, 2, 3, 4, 5, 6, 7 };
		r.rotate(arr, 7, 2);

	}

}
