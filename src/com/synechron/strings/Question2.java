package com.synechron.strings;

import java.util.Arrays;
import java.util.Scanner;

public class Question2 {

	static boolean isAnagram(String str1, String str2) {

		if (str1.length() != str2.length()) {
			return false;
		} else {

			char[] array1 = str1.toCharArray();
			char[] array2 = str2.toCharArray();
			Arrays.sort(array1);
			Arrays.sort(array2);
			for (int i = 0; i < str1.length(); i++) {
				if (array1[i] != array2[i]) {
					return false;

				}
			}
			return true;

		}

	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first string");
		String str1 = sc.nextLine();
		System.out.println("Enter second string");
		String str2 = sc.nextLine();
		boolean result = isAnagram(str1, str2);
		if (result) {
			System.out.println("Given strins are anagrams");
		} else {
			System.out.println("Given strins are not anagrams");
		}

	}

}
