package com.synechron.arrays;

public class SecondLargestElement {

	static void findSecondlargest(int arr[], int n) {
		int i, first, second;
		if (n < 2) {

			System.out.println("Invalid Input");
		}

		first = second = Integer.MIN_VALUE;
		for (i = 0; i < n; i++) {
			if (arr[i] > first) {
				second = first;
				first = arr[i];
			}

			else if (arr[i] > second && arr[i] != first)
				second = arr[i];

		}

		if (second == Integer.MIN_VALUE)
			System.out.println("There is no second largest" + " element");
		else
			System.out.println("The second largest element" + " is " + second);

	}

	public static void main(String[] args) {
		int arr[] = { 12, 35, 1, 10, 34, 1 };
		int n = arr.length;
		findSecondlargest(arr, n);

	}

}
