package com.synechron.linkedlists;

import java.util.HashSet;

class LinkedList {
	static Node head;

	static class Node {

		int data;

		Node next;

		Node(int d) {
			this.data = d;
			this.next = null;
		}

	}

	boolean detectAndRemoveLoop(Node node) {
		Node slow = node;
		Node fast = node;
		while (slow != null && fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;

			// If slow and fast meet at same point then loop is present
			if (slow == fast) {
				return removeLoop(node);
			}
		}
		return false;
	}

	boolean removeLoop(Node h) {
		HashSet<Node> set = new HashSet<>();
		Node prev = null;
		while (h != null) {
			if (set.contains(h)) {
				prev.next = null;
				return true;
			} else {
				set.add(h);
				prev = h;
				h = h.next;

			}
		}
		return false;

	} 

	public static void main(String[] args) {

		LinkedList list = new LinkedList();
		list.head = new Node(50);
		list.head.next = new Node(20);
		list.head.next.next = new Node(15);
		list.head.next.next.next = new Node(4);
		list.head.next.next.next.next = new Node(10);

		// Creating a loop for testing
		head.next.next.next.next.next = head.next.next;
		list.detectAndRemoveLoop(head);
		System.out.println("After removing loop");
		while (head != null) {
			System.out.println(head.data + " ");
			head = head.next;
		}

	}

}
