package com.synechron.arrays;

public class LargestSumSubArray {

	static int largestSum(int arr[], int n) {
		int maxSoFar = Integer.MIN_VALUE, maxEndsndsHere = 0;
		for (int i = 0; i < n; i++) {
			maxEndsndsHere = maxEndsndsHere + arr[i];
			if (maxSoFar < maxEndsndsHere)
				maxSoFar = maxEndsndsHere;
			if (maxEndsndsHere < 0)
				maxEndsndsHere = 0;
		}
		return maxSoFar;

	}

	public static void main(String[] args) {
		int[] a = { -2, -3, 4, -1, -2, 1, 5, -3 };
		System.out.println("Maximum contiguous sum is " + largestSum(a, a.length));
	}

}
